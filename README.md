# Amiga MAS Player Pro2+  
## ("Fuck Corona" QFP Edition 2022)  
### If you bought this thing at sordan.ie you have been cheated! Sordan.ie is selling this without permission!
---  
This is a MP3 Player hardware for the parallelport of the Amiga Computer.  
It plays any kind of MP3 files and MP3 radio streams without stressing the cpu.  

The MAS player is a small module that takes over the task of decoding music  
compressed in MPEG and providing the resulting data as an analog signal.  
The decompression is done by a chip from Micronas Intermetall. This chip named  
'MAS 3507D' is designed to decode different standards. The most commonly used  
standard is MPEG 1, Layer 3, which is called 'MP3' for short. The sound chip CS4331KS  
and its successor CS4334 from Crystal Cirrus Logic then generate the necessary analog  
signal, which can be fed to an amplifier or a stereo system.  
It has an Audio_in for bypassing Paula Sound from the RCAs.  
  
  
  
### Connections  
Audio_in   bypass Paula Sound comming from the RCA Jacks  
Audio_out  connect to your Stereo Equipment  
DB25 Connector  connect to the parallelport of your Amiga Computer  
USB Port  for 5V Power  
  
### The Hardware
![Working](Pics/22-02-24 20-19-36 0903.jpg)  
![AmigaAmp in Action](Pics/22-02-12 21-47-38 0874.jpg)  
  
### Special Parts  
D200 can be a CS4331KS or CS4334KS  
D100 is the QFP-44 Version of MAS3507D  
  
  
### Recommend Software for MAS Player  
  
[The MHI Software](http://aminet.net/package/mus/play/mhi_dev "MHI Plugin")  
[The MHI MASPro Driver](https://aminet.net/package/driver/audio/mhi_MASPro "MHI MASPro Driver")  
[The MPEGA Library](https://aminet.net/package/util/libs/mpega_library "MPEGA Library")  
[AmigaAMP 68k](http://www.amigaamp.de/download.shtml "AmigaAmp")  
  
  
### Credits to
  
[Dirk Conrad](https://www.mas-player.de/cgi-bin/mas-player.pl?site=mp3_hardware "The original Mas-Player-Homepage")  
[A1k - Amiga Forum](https://www.a1k.org "The best amigaforum worldwide")  
  
  
### Original Projectpage  
https://gitlab.com/marceljaehne/amiga-mas-player-pro2  
  
**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**  

